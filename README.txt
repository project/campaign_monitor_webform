Campaign Monitor Webform Handler

INTRODUCTION
------------

Campaign Monitor Webform module provides a webforms handler to submit user data to Campaign Monitor.

See: https://www.campaignmonitor.com/

REQUIREMENTS
------------

This module requires the following modules:

 * [Campaign Monitor REST API Client](https://www.drupal.org/project/campaign_monitor_rest_client)
 * [Webform] (https://www.drupal.org/project/webform)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

 * Setup the API Key at  Configuration » Web services » Campaign Monitor Rest Client Settings. (/admin/config/services/campaign_monitor_rest_client)
 * Add a new submition handler to the webform. Choose Campaign Monitor, set the Client List and fields mapping.
 * Optionally choose the triggering element (usually checkbox) to subscribe users depending on it's value.

MAINTAINERS
-----------

Current maintainers:
 * Lev Ananikyan (le72) - https://www.drupal.org/user/1866896
