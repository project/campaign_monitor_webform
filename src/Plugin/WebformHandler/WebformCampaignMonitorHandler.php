<?php

namespace Drupal\campaign_monitor_webform\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Webform submission remote post handler.
 *
 * @WebformHandler(
 *   id = "campaign_monitor_handler",
 *   label = @Translation("Campaign Monitor"),
 *   category = @Translation("External"),
 *   description = @Translation("Creates a Campaign Monitor subscription on form submission."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class WebformCampaignMonitorHandler extends WebformHandlerBase {

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The Campaign Monitor RESTClient.
   *
   * @var \Drupal\CampaignMonitorRestClientFactory
   */
  protected $campaignMonitorRESTClient;


  /**
   * Customer fields.
   *
   * These fields we need to submit.
   *
   * @var array
   */
  protected $customerFields = [
    'email' => 'E-mail',
    'firstName' => 'First Name',
    'lastName' => 'Last Name',
    'mobileNumber' => 'Mobile Number',
  ];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->loggerFactory = $container->get('logger.factory')->get('webform_campaign_monitor');
    $instance->campaignMonitorRESTClient = $container->get('campaign_monitor_rest_client');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'customer' => [],
      'list' => '',
      'trigger' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $webform = $this->getWebform();

    $map_sources = [];
    $elements = $this->webform->getElementsInitializedAndFlattened();
    foreach ($elements as $key => $element) {
      if (strpos($key, '#') === 0 || empty($element['#title']) || !empty($element['#webform_composite_elements'])) {
        if (!empty($element['#webform_composite_elements'])) {
          foreach ($element['#webform_composite_elements'] as $subkey => $subelement) {
            $map_sources[$key . '__' . $subkey] = $element['#title'] . ' - ' . $subelement['#title'];
          }
        }
        continue;
      }
      $map_sources[$key] = $element['#title'];
    }
    $field_definitions = $this->submissionStorage->getFieldDefinitions();
    $field_definitions = $this->submissionStorage->checkFieldDefinitionAccess($webform, $field_definitions);
    foreach ($field_definitions as $key => $field_definition) {
      $map_sources[$key] = $field_definition['title'] . ' (type : ' . $field_definition['type'] . ')';
    }

    $form['list'] = [
      '#type' => 'select',
      "#title" => $this->t('Subscribers list'),
      "#options" => $this->getClientsListsOptions(),
      '#default_value' => $this->configuration['list'],
    ];

    $form['trigger'] = [
      '#type' => 'select',
      '#title' => $this->t('Triggering field'),
      '#description' => $this->t('Only if selected field has value/checked the data will be
       submitted to CampaignMonitor. Chose Always to skip the check.'),
      '#options' => ["0" => "-- Always --"] + $map_sources,
      '#default_value' => $this->configuration['trigger'],
    ];

    $form['customer'] = [
      '#type' => 'webform_mapping',
      '#title' => $this->t('Customer mapping'),
      '#description' => $this->t('Only Maps with specified "CampaignMonitor" will be submitted to Campaign Monitor.'),
      '#source__title' => $this->t('Webform submitted data'),
      '#destination__title' => $this->t('Campaign Monitor field'),
      '#source' => $map_sources,
      '#destination__type' => 'webform_select_other',
      '#destination' => $this->customerFields,
      '#default_value' => $this->configuration['customer'],
    ];

    return $form;
  }

  /**
   * Config form submit.
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
  }

  /**
   * PreSave of the form.
   */
  public function preSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    $listID = $this->configuration['list'];
    $wf_submission = $webform_submission->getRawData();
    $email = $this->getMappedValue("email", $webform_submission);
    $firstName = $this->getMappedValue("firstName", $webform_submission);
    $lastName = $this->getMappedValue("lastName", $webform_submission);
    $mobileNumber = $this->getMappedValue("mobileNumber", $webform_submission);

    // Build data array.
    $data = [
      'EmailAddress' => $email,
      "Name" => ($firstName ?? "") . ($lastName ? " " . $lastName : ""),
      "MobileNumber" => $mobileNumber ?? "",
      "ConsentToTrack" => "Yes",
      "Resubscribe" => TRUE,
      "RestartSubscriptionBasedAutoresponders" => TRUE,
    ];
    // Custom fields.
    $customFields = $this->getCustomFieldsValues($webform_submission);
    if (!empty($customFields)) {
      $data['CustomFields'] = $customFields;
    }
    $send = TRUE;
    if ($this->configuration['trigger']) {
      if (!$wf_submission[$this->configuration['trigger']]) {
        $send = FALSE;
      }
    }

    if ($send) {
      try {
        $response = $this->campaignMonitorRESTClient->post('subscribers/' . $listID . '.json', ['json' => $data]);
        $this->loggerFactory->info("Subscription have sent to Campaign Monitor. Response: %response", ['%response' => print_r($response, TRUE)]);
      }
      catch (ClientException $exception) {
        \Drupal::messenger()->addMessage($this->t("Something went wrong with your subscription. %message", ['%message' => $exception->getMessage()]), "warning");
      }
    }
    else {
      $this->loggerFactory->info("Subscription have not sent to Campaign Monitor, because of %trigger value: %value",
      ['%trigger' => $this->configuration['trigger'], '%value' => print_r($wf_submission[$this->configuration['trigger']], TRUE)]);
    }

    parent::preSave($webform_submission);
  }

  /**
   * Helper function to get dropdown options.
   *
   * @return array
   *   Options for List dropdown
   */
  protected function getClientsListsOptions() {
    $options = [];
    $clients = $this->campaignMonitorRESTClient->get('clients.json')->getBody()->getContents();
    foreach (json_decode($clients) as $client) {
      $options[$client->Name] = [];
      $clientLists = $this->campaignMonitorRESTClient->get('clients/' . $client->ClientID . '/lists.json')->getBody()->getContents();
      foreach (json_decode($clientLists) as $clientList) {
        $options[$client->Name][$clientList->ListID] = $clientList->Name;
      }
    }
    return $options;
  }

  /**
   * Extract submitted value.
   */
  private function getMappedValue($field, $webformSubmission) {
    $data = $webformSubmission->getRawData();
    $fieldKey = array_search($field, $this->configuration['customer']);
    if ($fieldKey === FALSE) {
      return NULL;
    }
    if (str_contains($fieldKey, "__")) {
      [$key, $subKey] = explode("__", $fieldKey);
      return $data[$key][$subKey];
    }
    else {
      return $data[$fieldKey];
    }
  }

  /**
   * Get Custom fields values in a format suitable to be submitted.
   */
  protected function getCustomFieldsValues($webformSubmission) {
    $customFieldsData = [];
    $webformData = $webformSubmission->getRawData();
    foreach ($this->configuration['customer'] as $formField => $destinationField) {
      if (!array_key_exists($destinationField, $this->customerFields)) {
        $customFieldsData[] = [
          "Key" => $destinationField,
          "Value" => $webformData[$formField],
        ];
      }
    }
    return $customFieldsData;
  }

}
